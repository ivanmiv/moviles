package com.example.fitnessapp.fitnessapp;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

public class CrearAlimento extends AppCompatActivity {

    private String unidadRacion;
    private EditText nombreAlimento;
    private EditText racionAlimento;
    private EditText caloriasAlimento;
    private EditText proteinasAlimento;
    private EditText grasasAlimento;
    private EditText carbsAlimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_alimento);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Spinner spinner = findViewById(R.id.spinerCrearComida);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.unidadCrearAlimento, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                unidadRacion = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void tomarDatos() {
        nombreAlimento = findViewById(R.id.etxt_nombre);
        racionAlimento = findViewById(R.id.etxt_racion);
        caloriasAlimento = findViewById(R.id.etxt_calorias);
        proteinasAlimento = findViewById(R.id.etxt_proteinas);
        grasasAlimento = findViewById(R.id.etxt_grasas);
        carbsAlimento = findViewById(R.id.etxt_carbs);

        if (!nombreAlimento.getText().toString().isEmpty()
                && !racionAlimento.getText().toString().isEmpty()
                && !caloriasAlimento.getText().toString().isEmpty()
                && !proteinasAlimento.getText().toString().isEmpty()
                && !grasasAlimento.getText().toString().isEmpty()
                && !carbsAlimento.getText().toString().isEmpty()) {

            String nombre = nombreAlimento.getText().toString();
            Double racion = Double.parseDouble(racionAlimento.getText().toString());
            Double calorias = Double.parseDouble(caloriasAlimento.getText().toString());
            Double proteinas = Double.parseDouble(proteinasAlimento.getText().toString());
            Double grasas = Double.parseDouble(grasasAlimento.getText().toString());
            Double carbs = Double.parseDouble(carbsAlimento.getText().toString());

            crearAlimento(nombre, racion, calorias, proteinas, grasas, carbs);


        } else {
            FancyToast.makeText(CrearAlimento.this, "Todos los campos son obligatorios", FancyToast.LENGTH_SHORT, FancyToast.ERROR, false).show();
        }
    }


    public void crearAlimento(String nombre, Double racion, Double calorias, Double proteinas, Double grasas, Double carbs) {

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put("nombre", nombre);
            values.put("unidad_racion", unidadRacion);
            values.put("racion_estandar", racion);
            values.put("energia", calorias);
            values.put("grasas", grasas);
            values.put("carbohidratos", carbs);
            values.put("proteinas", proteinas);
            values.put("image", R.drawable.banana);

            db.insert("alimento", null, values);
            db.close();

            limpiarCampos();

            FancyToast.makeText(CrearAlimento.this, "Alimento creado con éxito", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();


        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void registrarAlimento(View view) {
        tomarDatos();
    }

    public void limpiarCampos(){
        nombreAlimento.setText("");
        racionAlimento.setText("");
        caloriasAlimento.setText("");
        proteinasAlimento.setText("");
        grasasAlimento.setText("");
        carbsAlimento.setText("");
    }
}

package com.example.fitnessapp.fitnessapp;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.ArrayList;

public class AgregaringresienteActivity extends AppCompatActivity {

    private Dialog confirmacion;
    private ArrayList<Integer> idsIngredientes = new ArrayList<Integer>();
    private int id_alimento;
    private String nombreReceta="";
    private ImageView closeConfirmacion, imagenConfirmacion;
    private TextView tituloConfirmacion, textoCalorias, textoRacion, textoProteinas, textoGrasas, textoCarbs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agrergaringresiente);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);

        final ListView listView = (ListView) findViewById(R.id.lv_ingredientes);
        llenarListView(listView);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                id_alimento = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                dialogoConfirmacion();
            }
        });


        confirmacion = new Dialog(this);
        confirmacion.setContentView(R.layout.activity_confirmacion_agregar_ingrediente);

        imagenConfirmacion = confirmacion.findViewById(R.id.imagePopUp);
        tituloConfirmacion = confirmacion.findViewById(R.id.textTituloPopUp);
        textoCalorias = confirmacion.findViewById(R.id.txt_calorias);
        textoRacion = confirmacion.findViewById(R.id.txt_racion);
        textoProteinas = confirmacion.findViewById(R.id.txt_proteinas);
        textoCarbs = confirmacion.findViewById(R.id.txt_carbs);
        textoGrasas = confirmacion.findViewById(R.id.txt_grasas);

        closeConfirmacion = (ImageView) confirmacion.findViewById(R.id.cerrarPopUp);
        closeConfirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
            }
        });


        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null) {
            nombreReceta = bd.getString("nombre");
            idsIngredientes = bd.getIntegerArrayList("idsIngredientes");
            System.out.println("tamano recibido " + idsIngredientes.size());
        }

        Button btn_agregarIngrediente = confirmacion.findViewById(R.id.btn_confirmarAgregaringrediente);
        btn_agregarIngrediente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
                guardarIngrediente(id_alimento);
            }
        });


        Button btn_listo = findViewById(R.id.btn_listoIngredientes);
        btn_listo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CrearRecetaActivity.class);
                intent.putExtra("nombre",nombreReceta);
                intent.putExtra("ingredientes",idsIngredientes);

                startActivity(intent);
                finish();
            }
        });


    }

    private void guardarIngrediente(int id_alimento) {
        idsIngredientes.add(id_alimento);
        confirmacion.dismiss();
        FancyToast.makeText(this, "Ingrediente agregado con éxito", Toast.LENGTH_SHORT,FancyToast.SUCCESS,false).show();
        System.out.println("tamaño ingredientes "+idsIngredientes.size());
    }

    public void llenarListView(ListView listView) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, energia, image, unidad_racion from alimento", null);

            System.out.println("count " + cursor.getCount());

            CursorAdapterComida cursoradap = new CursorAdapterComida(this, cursor);

            listView.setAdapter(cursoradap);
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void dialogoConfirmacion(){
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, unidad_racion, energia, grasas, carbohidratos, proteinas, image from alimento where _id="+id_alimento, null);


            if(cursor.moveToFirst()) {
                String nombre = cursor.getString(1);
                double racion = cursor.getDouble(2);
                String unidad_racion = cursor.getString(3);
                double energia = cursor.getDouble(4);
                double grasas = cursor.getDouble(5);
                double carbs = cursor.getDouble(6);
                double proteinas = cursor.getDouble(7);
                int imagen = cursor.getInt(8);

                tituloConfirmacion.setText(nombre);
                textoCalorias.setText(energia + " cal");
                textoRacion.setText(racion + unidad_racion);
                textoProteinas.setText(proteinas + " gr");
                textoCarbs.setText(carbs + " gr");
                textoGrasas.setText(grasas + " gr");
                imagenConfirmacion.setImageResource(imagen);
            }

            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        confirmacion.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmacion.show();
    }
}

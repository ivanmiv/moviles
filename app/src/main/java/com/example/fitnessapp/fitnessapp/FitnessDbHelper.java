package com.example.fitnessapp.fitnessapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by john on 14/05/18.
 */

public class FitnessDbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "moviles";
    private static final int DB_VERSION = 2;

    private static final String CREAR_TABLA_ALIMENTO =
            "CREATE TABLE alimento ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "nombre TEXT NOT NULL,"
                    + "unidad_racion TEXT NOT NULL,"
                    + "racion_estandar REAL NOT NULL,"
                    + "energia REAL NOT NULL,"
                    + "grasas REAL NOT NULL," //gr
                    + "carbohidratos REAL NOT NULL," //gr
                    + "image INTEGER NOT NULL,"
                    + "proteinas REAL NOT NULL)"; //gr

    private static final String CREAR_TABLA_ALIMENTO_USUARIO =
            "CREATE TABLE alimento_usuario ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "fecha DATETIME DEFAULT CURRENT_DATE NOT NULL, "
                    + "cantidad INTEGER NOT NULL, "
                    + "horario_comida TEXT NOT NULL," //Desayuno, comida, cena, snack
                    + "alimento_id INTEGER, "
                    + "FOREIGN KEY (alimento_id) REFERENCES alimento(_id))";


    private static final String CREAR_TABLA_RECETA = "CREATE TABLE receta ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "nombre TEXT NOT NULL,"
            + "unidad_racion TEXT NOT NULL,"
            + "racion_estandar REAL NOT NULL,"
            + "energia REAL NOT NULL,"
            + "grasas REAL NOT NULL," //gr
            + "carbohidratos REAL NOT NULL," //gr
            + "image INTEGER NOT NULL,"
            + "proteinas REAL NOT NULL)"; //gr

    private static final String CREAR_TABLA_RECETA_ALIMENTO = "CREATE TABLE receta_alimento ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "alimento INTEGER,"
            + "receta INTEGER,"
            + "FOREIGN KEY (alimento) REFERENCES alimento(_id),"
            + "FOREIGN KEY (receta) REFERENCES receta(_id))";

    private static final String CREAR_TABLA_RECETA_USUARIO = "CREATE TABLE receta_usuario ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT,"
            + "fecha TEXT NOT NULL,"
            + "cantidad INTEGER NOT NULL,"
            + "horario_receta TEXT NOT NULL,"
            + "receta INTEGER,"
            + "FOREIGN KEY (receta) REFERENCES receta(_id))";


    private static final String CREAR_TABLA_INFO_PERSONAL =
            "CREATE TABLE informacion_personal ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "fecha_nacimiento DATETIME DEFAULT CURRENT_DATE NOT NULL, "
                    + "sexo TEXT NOT NULL, " // F, M
                    + "altura REAL NOT NULL, "
                    + "peso REAL NOT NULL, " //cm
                    + "edad INTEGER NOT NULL, "
                    + "objetivo TEXT NOT NULL, " //Mantener peso, Subir de peso, Bajar de peso
                    + "actividad_fisica TEXT NOT NULL)"; //Poco o ningún ejercicio, Ejercicio ligero
    //Ejercicio moderado, Ejercicio fuerte, Ejercicio muy fuerte


//    private static final String CREAR_TABLA_USUARIO = "CREATE TABLE usuario ("
//            + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
//            + "edad TEXT NOT NULL,"
//            + "objetivo INTEGER NOT NULL,"
//            + "peso_deseado REAL NOT NULL,"
//            + "peso_inicial REAL NOT NULL,"
//            + "nivel_actividad TEXT NOT NULL)";

    private static final String CREAR_TABLA_EJERCICIO = "CREATE TABLE ejercicio ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "nombre TEXT NOT NULL,"
            + "duracion INTEGER NOT NULL,"
            + "calorias_quemadas INTEGER NOT NULL)";
//            + "FOREIGN KEY (usuario) REFERENCES usuario(id))";

    private static final String CREAR_TABLA_EJERCICIO_USUARIO =
            "CREATE TABLE ejercicio_usuario ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "fecha DATETIME DEFAULT CURRENT_DATE NOT NULL, "
                    + "cantidad INTEGER NOT NULL, "
                    + "ejercicio_id INTEGER, "
                    + "FOREIGN KEY (ejercicio_id) REFERENCES ejercicio(_id))";

    private static final String CREAR_TABLA_HISTORICO_PESO = "CREATE TABLE historico_peso ("
            + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "fecha TEXT NOT NULL,"
            + "peso INTEGER NOT NULL,"
            + "FOREIGN KEY (usuario) REFERENCES usuario(id))";


    public FitnessDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("PRAGMA foreign_keys = ON;");
        sqLiteDatabase.execSQL(CREAR_TABLA_ALIMENTO);
        sqLiteDatabase.execSQL(CREAR_TABLA_ALIMENTO_USUARIO);
        sqLiteDatabase.execSQL(CREAR_TABLA_INFO_PERSONAL);
        sqLiteDatabase.execSQL(CREAR_TABLA_EJERCICIO);
        sqLiteDatabase.execSQL(CREAR_TABLA_EJERCICIO_USUARIO);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA_ALIMENTO);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA_USUARIO);

        /*sqLiteDatabase.execSQL(CREAR_TABLA_USUARIO);

        sqLiteDatabase.execSQL(CREAR_TABLA_HISTORICO_PESO);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA_ALIMENTO);
        sqLiteDatabase.execSQL(CREAR_TABLA_RECETA_USUARIO);*/

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        actualizarBD(sqLiteDatabase, 1, DB_VERSION);
    }

    /**
     * Permite actualizar la BD según la versión que se este ingresando
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    public void actualizarBD(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > 1) {
            db.execSQL("DROP TABLE  IF EXISTS alimento;");
            db.execSQL("DROP TABLE  IF EXISTS ejercicio;");
            db.execSQL("DROP TABLE  IF EXISTS alimento_usuario;");
            db.execSQL("DROP TABLE  IF EXISTS ejercicio_usuario;");
            db.execSQL("DROP TABLE  IF EXISTS informacion_personal;");
            db.execSQL("DROP TABLE  IF EXISTS receta;");
            db.execSQL("DROP TABLE  IF EXISTS receta_alimento;");
            db.execSQL("DROP TABLE  IF EXISTS receta_usuario;");
            db.execSQL(CREAR_TABLA_ALIMENTO);
            db.execSQL(CREAR_TABLA_ALIMENTO_USUARIO);
            db.execSQL(CREAR_TABLA_INFO_PERSONAL);
            db.execSQL(CREAR_TABLA_EJERCICIO);
            db.execSQL(CREAR_TABLA_EJERCICIO_USUARIO);
            db.execSQL(CREAR_TABLA_RECETA);
            db.execSQL(CREAR_TABLA_RECETA_ALIMENTO);
            db.execSQL(CREAR_TABLA_RECETA_USUARIO);
        }

    }

    public void insertarAlimento(Context context, String name, double racion, double energia, double grasas, double carbs, double proteinas, int image, String unidad) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(context);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put("nombre", name);
            values.put("racion_estandar", racion);
            values.put("energia", energia);
            values.put("grasas", grasas);
            values.put("carbohidratos", carbs);
            values.put("proteinas", proteinas);
            values.put("image", image);
            values.put("unidad_racion", unidad);

            db.insert("alimento", null, values);
            db.close();
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void insertarReceta(Context context, String name, double racion, double energia, double grasas, double carbs, double proteinas, int image, String unidad) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(context);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put("nombre", name);
            values.put("racion_estandar", racion);
            values.put("energia", energia);
            values.put("grasas", grasas);
            values.put("carbohidratos", carbs);
            values.put("proteinas", proteinas);
            values.put("image", image);
            values.put("unidad_racion", unidad);

            db.insert("receta", null, values);
            db.close();
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    private static void insertarAlimento(SQLiteDatabase db, String name, int racion, int energia, int grasas, int carbohidratos, int proteinas) {
        ContentValues valores_alimentos = new ContentValues();
        valores_alimentos.put("nombre", name);
        valores_alimentos.put("reacion_estandar", racion);
        valores_alimentos.put("energia", energia);
        valores_alimentos.put("grasas", grasas);
        valores_alimentos.put("carbohidratos", carbohidratos);
        valores_alimentos.put("proteinas", proteinas);
        db.insert("alimento", null, valores_alimentos);
    }

    /**
     * Registra una rutina en la BD
     *
     * @param db
     * @param nombre
     * @param duracion
     * @param calorias_quemadas
     */
    public static void registrarRutina(SQLiteDatabase db, String nombre, int duracion, int calorias_quemadas) {
        ContentValues valores_ejercicio = new ContentValues();
        valores_ejercicio.put("nombre", nombre);
        valores_ejercicio.put("duracion", duracion);
        valores_ejercicio.put("calorias_quemadas", calorias_quemadas);

        db.insert("ejercicio", null, valores_ejercicio);
    }

    /**
     * Elimina una rutina de la base de datos
     *
     * @param db
     * @param id_rutina
     */
    public static void eliminarRutina(SQLiteDatabase db, int id_rutina) {
        db.delete("ejercicio", "_id=?", new String[]{id_rutina + ""});
    }

    /**
     * Para registrar fecha actual
     */
    private static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public static void registrarComida(SQLiteDatabase db, int alimento_id, String horario, Context context) {
        ContentValues valores_registro = new ContentValues();
        valores_registro.put("fecha", FitnessDbHelper.getDateTime());
        valores_registro.put("cantidad", 1);
        valores_registro.put("alimento_id", alimento_id);
        valores_registro.put("horario_comida", horario);
        System.out.println(FitnessDbHelper.getDateTime() + " " + horario + " " + alimento_id);
        Long idResultante = db.insert("alimento_usuario", null, valores_registro);
    }

    public static void registrarReceta(SQLiteDatabase db, int receta_id, String horario, Context context) {
        ContentValues valores_registro = new ContentValues();
        valores_registro.put("fecha", FitnessDbHelper.getDateTime());
        valores_registro.put("cantidad", 1);
        valores_registro.put("receta", receta_id);
        valores_registro.put("horario_receta", horario);
        System.out.println(FitnessDbHelper.getDateTime() + " " + horario + " " + receta_id);
        Long idResultante = db.insert("receta_usuario", null, valores_registro);
    }

    /**
     * Realiza un registro a la tabla ejercicio_usuario
     *
     * @param db
     * @param ejercicio_id
     * @param context
     */
    public static void registrarEjercicioUsuario(SQLiteDatabase db, int ejercicio_id, Context context) {
        ContentValues valores_registro = new ContentValues();
        valores_registro.put("fecha", FitnessDbHelper.getDateTime());
        valores_registro.put("cantidad", 1);
        valores_registro.put("ejercicio_id", ejercicio_id);
        db.insert("ejercicio_usuario", null, valores_registro);
        FancyToast.makeText(context, "Se registró el ejercicio exitosamente", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();
    }

    public static void registrarInformacionPersonal(SQLiteDatabase db, String fecha, String sexo, double altura, double peso,
                                                    int edad, String objetivo, String actividad_fisica, Context context) {
        ContentValues valores_registro = new ContentValues();
        valores_registro.put("fecha_nacimiento", fecha);
        valores_registro.put("sexo", sexo);
        valores_registro.put("altura", altura);
        valores_registro.put("peso", peso);
        valores_registro.put("edad", edad);
        valores_registro.put("objetivo", objetivo);
        valores_registro.put("actividad_fisica", actividad_fisica);
        db.insert("informacion_personal", null, valores_registro);
    }

    public static int numeroDatosInformacionPersonal(SQLiteDatabase db) {
        Cursor cursor = db.query("informacion_personal", null, null, null, null, null, null);
        return cursor.getCount();
    }

    public static void actualizarInformacionPersonal(SQLiteDatabase db, double peso, String objetivo, String actividad_fisica, int id_informacion) {
        ContentValues valores_registro = new ContentValues();
        valores_registro.put("peso", peso);
        valores_registro.put("objetivo", objetivo);
        valores_registro.put("actividad_fisica", actividad_fisica);
        db.update("informacion_personal", valores_registro, "_id=?", new String[]{id_informacion + ""});
    }

    static String makePlaceholders(int len) {
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            throw new RuntimeException("No placeholders");
        } else {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++) {
                sb.append(",?");
            }
            return sb.toString();
        }
    }

}

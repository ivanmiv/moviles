package com.example.fitnessapp.fitnessapp;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

public class CrearRutinaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_rutina);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
        final TextView textCalorias = (TextView) findViewById(R.id.texValor);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textCalorias.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    /**
     * Limbia los campos de texto
     * @return
     */
    public void limpiarCampos() {
        SeekBar barra = (SeekBar) findViewById(R.id.seekBar);
        TextView tv_calorias = (TextView) findViewById(R.id.texValor);
        TextInputLayout ti_duracion = (TextInputLayout) findViewById(R.id.input_layout_duracion);
        TextInputLayout ti_nombre = (TextInputLayout) findViewById(R.id.input_layout_nombre);
        barra.setProgress(0);
        tv_calorias.setText("");
        ti_duracion.getEditText().setText("");
        ti_nombre.getEditText().setText("");
    }

    /**
     * Verifica si los campos estan vacios
     * @return
     */
    public boolean camposVacios(){
        TextView tv_calorias = (TextView) findViewById(R.id.texValor);
        TextInputLayout ti_duracion = (TextInputLayout) findViewById(R.id.input_layout_duracion);
        TextInputLayout ti_nombre = (TextInputLayout) findViewById(R.id.input_layout_nombre);
        boolean val = TextUtils.isEmpty(tv_calorias.getText()) ;
        return (TextUtils.isEmpty(tv_calorias.getText()) || TextUtils.isEmpty(ti_duracion.getEditText().getText()) || TextUtils.isEmpty(ti_nombre.getEditText().getText())) ? true: false;

    }

    public void guardarRutina(View view) {

        if (!camposVacios()){
            SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
            try {
                SQLiteDatabase db = dbhelper.getWritableDatabase();
                TextView tv_calorias = (TextView) findViewById(R.id.texValor);
                TextInputLayout ti_duracion = (TextInputLayout) findViewById(R.id.input_layout_duracion);
                TextInputLayout ti_nombre = (TextInputLayout) findViewById(R.id.input_layout_nombre);
                String nombre = ti_nombre.getEditText().getText().toString();
                String[] arreglo_duracion = ti_duracion.getEditText().getText().toString().split(":");
                int duracion = (Integer.parseInt(arreglo_duracion[0].replaceFirst(" ", "")) * 60) + Integer.parseInt(arreglo_duracion[1].replaceFirst(" ", ""));

                FitnessDbHelper.registrarRutina(db, nombre, duracion, Integer.parseInt((String) tv_calorias.getText()));
                limpiarCampos();
                FancyToast.makeText(this, "Se registró la rutina exitosamente", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();

            } catch (SQLiteException e) {
                FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
            }
        }else{
            FancyToast.makeText(this, "No pueden haber campos en blanco", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
        }
    }
}

package com.example.fitnessapp.fitnessapp;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class BarChartAdapter extends RecyclerView.Adapter<BarChartAdapter.ViewHolder> {
    ArrayList<BarData> list ;
    int is_horizontal;

    /**
     * Constructor
     * @param items
     */
    public BarChartAdapter(ArrayList<BarData> items, int alineacion) {
        this.is_horizontal = alineacion;
        this.list = items;
    }

    @NonNull
    @Override
    public BarChartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (is_horizontal == 1){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_barchart, parent, false);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_barchart_vertical, parent, false);
        }

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    /*
     * Setea los valores para cada uno de los items del recycler view
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull BarChartAdapter.ViewHolder holder, final int position) {
        holder.grafico.setData(list.get(position));
        BarData data = (BarData) holder.grafico.getData();

        data.setValueTextColor(Color.BLACK);
        holder.grafico.getDescription().setEnabled(false);
        holder.grafico.setDrawGridBackground(false);
        XAxis xAxis = holder.grafico.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        xAxis.setAxisMinimum(0f);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            String[] labels = new String[] {
                    "", "Proteinas", "Carbohidratos", "Grasas"
            };
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return labels[(int) value % labels.length];
            }
        });

        YAxis leftAxis = holder.grafico.getAxisLeft();
        leftAxis.setLabelCount(5, false);
        leftAxis.setSpaceTop(15f);

        YAxis rightAxis = holder.grafico.getAxisRight();
        rightAxis.setLabelCount(5, false);
        rightAxis.setSpaceTop(15f);

        // set data
        holder.grafico.setData(data);
        holder.grafico.setFitBars(true);

        // do not forget to refresh the chart
//            holder.chart.invalidate();
        holder.grafico.animateY(700);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    // Provee una referencia a las vistas de cada item
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public BarChart grafico;


        /**
         * Pasamos una vista a nuestro constructor para poder buscar los elementos usando findViewById
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            grafico = (BarChart) itemView.findViewById(R.id.chart);

        }
    }
}

package com.example.fitnessapp.fitnessapp;


import android.app.Dialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;


/**
 * A simple {@link Fragment} subclass.
 */
public class IngresarRecetaFragment extends Fragment {

    private String seleccionReceta;
    private Dialog confirmacion;
    private ImageView closeConfirmacion, imagenConfirmacion;
    private TextView tituloConfirmacion, textoCalorias, textoRacion, textoProteinas, textoGrasas, textoCarbs;
    private Button btn_consumir;
    private int id_alimento;

    public IngresarRecetaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ingresar_receta, container, false);

        Spinner spinner = view.findViewById(R.id.spinerIngresarReceta);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.ingrComidaOptions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                /*Toast.makeText(adapterView.getContext(),
                        (String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show(); */
                seleccionReceta = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        final ListView listView = (ListView) view.findViewById(R.id.lv_recetas);
        llenarListView(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                id_alimento = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                dialogoConfirmacion();
            }
        });

        confirmacion = new Dialog(view.getContext());
        confirmacion.setContentView(R.layout.activity_confirmacion_ingresar_comida);

        btn_consumir = confirmacion.findViewById(R.id.btn_confirmarConsumirAlimento);
        imagenConfirmacion = confirmacion.findViewById(R.id.imagePopUp);
        tituloConfirmacion = confirmacion.findViewById(R.id.textTituloPopUp);
        textoCalorias = confirmacion.findViewById(R.id.txt_calorias);
        textoRacion = confirmacion.findViewById(R.id.txt_racion);
        textoProteinas = confirmacion.findViewById(R.id.txt_proteinas);
        textoCarbs = confirmacion.findViewById(R.id.txt_carbs);
        textoGrasas = confirmacion.findViewById(R.id.txt_grasas);

        closeConfirmacion = (ImageView) confirmacion.findViewById(R.id.cerrarPopUp);
        closeConfirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
            }
        });


        Button btn_confirmarConsumir = confirmacion.findViewById(R.id.btn_confirmarConsumirAlimento);
        btn_confirmarConsumir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
                registrarRecetaUsuario(id_alimento);

            }
        });

        Button btn_eliminarConsumir = confirmacion.findViewById(R.id.btn_confEliminar);
        btn_eliminarConsumir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
                eliminarReceta(id_alimento);
                listView.setAdapter(null);
                llenarListView(listView);
                FancyToast.makeText(getContext(), "Se eliminó la receta exitosamente", FancyToast.LENGTH_SHORT, FancyToast.WARNING, false).show();
            }
        });


        return view;
    }

    public void llenarListView(ListView listView) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, energia, image, unidad_racion from receta", null);


            CursorAdapterComida cursoradap = new CursorAdapterComida(getContext(), cursor);

            listView.setAdapter(cursoradap);
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }


    public void dialogoConfirmacion(){
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getView().getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, unidad_racion, energia, grasas, carbohidratos, proteinas, image from receta where _id="+id_alimento, null);
            if(cursor.moveToFirst()) {
                String nombre = cursor.getString(1);
                double racion = cursor.getDouble(2);
                String unidad_racion = cursor.getString(3);
                double energia = cursor.getDouble(4);
                double grasas = cursor.getDouble(5);
                double carbs = cursor.getDouble(6);
                double proteinas = cursor.getDouble(7);
                int imagen = cursor.getInt(8);

                tituloConfirmacion.setText(nombre);
                textoCalorias.setText(energia + " cal");
                textoRacion.setText(racion + unidad_racion);
                textoProteinas.setText(proteinas + " gr");
                textoCarbs.setText(carbs + " gr");
                textoGrasas.setText(grasas + " gr");
                imagenConfirmacion.setImageResource(imagen);
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        confirmacion.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmacion.show();
    }

    public void registrarRecetaUsuario(int alimento_id){
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            FitnessDbHelper.registrarReceta(db, alimento_id, seleccionReceta, getView().getContext());
            FancyToast.makeText(getView().getContext(), "Se registró la receta exitosamente", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public void eliminarReceta(int id_alimento){
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            db.delete("receta", "_id=?", new String[]{id_alimento + ""});
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }







}

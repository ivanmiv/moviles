package com.example.fitnessapp.fitnessapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportesFragment extends Fragment {
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private OptionsReportesAdapter optionsAdapter;
    private ArrayList<Opcion> opciones;


    public ReportesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_reportes, container, false);
        this.opciones = getAllOpciones();
        this.recyclerView = (RecyclerView) v.findViewById(R.id.recyclerOpcionesReportes);
        //Indica que el contenido no cambiará el tamaño del layout del RecyclerView (Mejor rendimiento)
        this.recyclerView.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getActivity());
        //Lista vertical
        this.layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recyclerView.setLayoutManager(layoutManager);
        this.optionsAdapter  = new OptionsReportesAdapter(opciones);
        this.recyclerView.setAdapter(optionsAdapter);



        return v;
    }

    private ArrayList<Opcion> getAllOpciones() {
        ArrayList<Opcion> list = new ArrayList<Opcion>();
        //Ingresar aqui las opciones del menú
        list.add(new Opcion("Consumo diario de macronutrientes", "", R.drawable.calendar));
        list.add(new Opcion("Consumo semanal de macronutrientes", "", R.drawable.diet));
        list.add(new Opcion("Consumo mensual de macronutrientes", "", R.drawable.watch));
        return list;
    }

}

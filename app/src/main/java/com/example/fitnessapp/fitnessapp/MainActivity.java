package com.example.fitnessapp.fitnessapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private String seleccion_sexo;
    private String seleccion_objetivo;
    private String seleccion_actividad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (!determinarActividadInicio()) {
            Intent intent = new Intent(this, InicioActivity.class);
            startActivity(intent);
            finish();
        }


        //Alimentos, rutinas y recetas iniciales en la base de datos
        llenarAlimentosIniciales();
        llenarRecetas();
        llenarRutinas();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final EditText et_fechaNacimiento = (EditText) findViewById(R.id.et_fechaNacimiento);
        et_fechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(et_fechaNacimiento);
            }
        });

        Spinner spinner_sexo = findViewById(R.id.spinner_sexo);
        Spinner spinner_objetivo = findViewById(R.id.spinner_objetivo);
        Spinner spinner_actividad_fisica = findViewById(R.id.spinner_actividad_fisica);

        ArrayAdapter<CharSequence> adapter_sexo = ArrayAdapter.createFromResource(this,
                R.array.sexoOptions, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_objetivo = ArrayAdapter.createFromResource(this,
                R.array.objetivoOptions, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_actividad = ArrayAdapter.createFromResource(this,
                R.array.actividadFisicaOptions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_sexo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_objetivo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_actividad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_sexo.setAdapter(adapter_sexo);
        spinner_objetivo.setAdapter(adapter_objetivo);
        spinner_actividad_fisica.setAdapter(adapter_actividad);

        spinner_sexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                seleccion_sexo = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner_objetivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                seleccion_objetivo = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner_actividad_fisica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                seleccion_actividad = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    /**
     * Permite visualizar un datePicker y asignarle el valor escogudo a un campo de texto
     *
     * @param et_date
     */
    private void showDatePickerDialog(final EditText et_date) {
        DatePickerFragment newFragment = DatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 porque enero es el cero
                final String selectedDate = year  + "-" + (month + 1) + "-" + day ;
                et_date.setText(selectedDate);
            }
        });
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }


    /**
     * Verifica si los campos estan vacios
     *
     * @return
     */
    public boolean camposVacios() {
        EditText et_fecha = (EditText) findViewById(R.id.et_fechaNacimiento);
        EditText et_peso = (EditText) findViewById(R.id.et_peso);
        EditText et_altura = (EditText) findViewById(R.id.et_altura);

        if (TextUtils.isEmpty(et_fecha.getText()) || TextUtils.isEmpty(et_peso.getText()) || TextUtils.isEmpty(et_altura.getText())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Limbia los campos de texto
     *
     * @return
     */
    public void limpiarCampos() {
        EditText et_fecha = (EditText) findViewById(R.id.et_fechaNacimiento);
        EditText et_peso = (EditText) findViewById(R.id.et_peso);
        EditText et_altura = (EditText) findViewById(R.id.et_altura);

        et_fecha.setText("");
        et_peso.setText("");
        et_altura.setText("");
    }

    public void guardarInformacionPersonal(View view) {
        if (!camposVacios()) {
            SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
            try {
                EditText et_fecha = (EditText) findViewById(R.id.et_fechaNacimiento);
                EditText et_peso = (EditText) findViewById(R.id.et_peso);
                EditText et_altura = (EditText) findViewById(R.id.et_altura);


                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Date fecha_actual = new Date();
                Date fecha_nacimiento = format.parse(et_fecha.getText().toString());

                SQLiteDatabase db = dbhelper.getReadableDatabase();
                FitnessDbHelper.registrarInformacionPersonal(db, et_fecha.getText().toString(), seleccion_sexo, Double.parseDouble(et_altura.getText().toString()),
                        Double.parseDouble(et_peso.getText().toString()),fecha_actual.getYear() - fecha_nacimiento.getYear(), seleccion_objetivo, seleccion_actividad, this);
                limpiarCampos();
                FancyToast.makeText(this, "Se registró su información personal exitosamente", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();
                db.close();
                Intent intent = new Intent(this, InicioActivity.class);
                startActivity(intent);
                finish();
            } catch (SQLiteException e) {
                FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
            } catch (ParseException e) {
                FancyToast.makeText(this, "Ocurrio un error calculando la fecha de nacimiento", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

            }
        } else {
            FancyToast.makeText(this, "No pueden haber campos en blanco", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
        }
    }

    public boolean determinarActividadInicio() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            if (FitnessDbHelper.numeroDatosInformacionPersonal(db) == 0) {
                return true;
            }
        } catch (SQLException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        } finally {
            dbhelper.close();
        }
        return false;
    }

    public void llenarAlimentosIniciales() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, energia, image, unidad_racion from alimento", null);

            if (cursor.getCount()==0){
                crearAlimentosIniciales();
            }

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void llenarRecetas() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, racion_estandar, energia, image, unidad_racion from receta", null);
            if (cursor.getCount()==0){
                crearRecetasIniciales();
            }
            /*cursor.close();*/

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void llenarRutinas() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select * from ejercicio", null);
            if (cursor.getCount()==0){
                crearRutinasIniciales(db);
            }
            /*cursor.close();*/

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void crearAlimentosIniciales(){
        FitnessDbHelper fitnessDbHelper = new FitnessDbHelper(this);
        fitnessDbHelper.insertarAlimento(this,"Jarra de cerveza",330, 	142, 0.0,11.7,1.5, R.drawable.cutlery, "ml");
        fitnessDbHelper.insertarAlimento(this,"Pastel con crema",126.0,350, 18.1,41.1,6.0,R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Pollo frito",16,49, 3.3,2.4,2.5, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Café",237,2, 0.1,0.0,0.3, R.drawable.cutlery, "ml");
        fitnessDbHelper.insertarAlimento(this,"Jugo de fruta",248,112, 0.5,25.8,1.7, R.drawable.cutlery, "ml");
        fitnessDbHelper.insertarAlimento(this,"Botella de agua",200,0.0, 0.0,0.0,0.0, R.drawable.cutlery, "ml");
        fitnessDbHelper.insertarAlimento(this,"Yogurt de fruta",245,243, 2.8,45.7,9.8, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Leche",244,149, 7.9,11.7,7.7, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Jamón",28,46, 2.4,1.1,4.6, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Almendras tostadas",30,182, 16.6,5.3,6.4, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Pan",28,74, 0.9,13.7,2.6, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Granola",60,293, 14.4,32.0,8.9, R.drawable.cutlery, "gr");
        fitnessDbHelper.insertarAlimento(this,"Galletas de avena",25,113, 4.5,17.2,1.6, R.drawable.cutlery, "gr");
    }

    public void crearRecetasIniciales(){
        FitnessDbHelper fitnessDbHelper = new FitnessDbHelper(this);
        fitnessDbHelper.insertarReceta(this,"Hamburguesa",110,279, 13.5,27.3,12.9, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Ensalda de verduras",138,22, 0.1,4.4,1.7,R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Pizza de pepperoni",111,313, 13.2,35.5,13.0, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Lasagna",123,166, 6.1,18.9,9.0, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Macarrones y queso",230,377, 11.5,53.1,15.4, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Sopa de verduras",252,149, 4.0,24.6,4.3, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Mousse de chocolate",150,338, 24.0,24.1,6.2, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Ensalada de frutas",264.0,312.0, 13.3,43.8,4.8, R.drawable.food, "gr");
        fitnessDbHelper.insertarReceta(this,"Pasta con salsa de tomate",252.0,176.0, 1.1,35.8,5.6, R.drawable.food, "gr");

    }

    public void crearRutinasIniciales(SQLiteDatabase db){
        FitnessDbHelper fitnessDbHelper = new FitnessDbHelper(this);
        fitnessDbHelper.registrarRutina(db,"Ciclismo",30,198);
        fitnessDbHelper.registrarRutina(db,"Pilates",30,103);
        fitnessDbHelper.registrarRutina(db,"Caminata",30,222);
        fitnessDbHelper.registrarRutina(db,"Correr",30,335);
        fitnessDbHelper.registrarRutina(db,"Yoga",30,92);
        fitnessDbHelper.registrarRutina(db,"Zumba",30,222);

    }

}

package com.example.fitnessapp.fitnessapp;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.ArrayList;

public class IngresarRutina extends AppCompatActivity {

    private Dialog confirmacion;
    private ImageView closeConfirmacion, imagenConfirmacion;
    private TextView tituloConfirmacion, textoDuracion, textoCalorias;
    private Button btn_anadir, btn_eliminar;
    private int id_ejercicio;
    private ListView list_rutinas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingresar_rutina);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.list_rutinas = (ListView) findViewById(R.id.lv_rutinas);


        llenarListView(list_rutinas);
        list_rutinas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                id_ejercicio = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
                dialogoConfirmacion();
            }
        });

        confirmacion = new Dialog(this);
        confirmacion.setContentView(R.layout.activity_confirmacion_ingresar_rutina);

        btn_anadir = confirmacion.findViewById(R.id.btn_confirmarAnadirEjercicio);
        btn_eliminar = confirmacion.findViewById(R.id.btn_eliminarEjercicio);

        imagenConfirmacion = confirmacion.findViewById(R.id.imagePopUp);
        tituloConfirmacion = confirmacion.findViewById(R.id.textTituloPopUpEjercicio);
        textoCalorias = confirmacion.findViewById(R.id.txt_calorias_ejercicio);
        textoDuracion = confirmacion.findViewById(R.id.txt_duracion);


        closeConfirmacion = (ImageView) confirmacion.findViewById(R.id.cerrarPopUp);
        closeConfirmacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmacion.dismiss();
            }
        });

    }

    /**
     * Despliega un modal de confirmación donde se puede añadir o eliminar el ejercicio al que se le ha hecho un clic
     */
    public void dialogoConfirmacion() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, duracion, calorias_quemadas from ejercicio where _id=" + id_ejercicio, null);


            if (cursor.moveToFirst()) {
                String nombre = cursor.getString(1);
                int duracion = cursor.getInt(2);
                int calorias = cursor.getInt(3);

                tituloConfirmacion.setText(nombre);
                textoDuracion.setText(String.valueOf(duracion));
                textoCalorias.setText(String.valueOf(calorias));
                imagenConfirmacion.setImageResource(R.drawable.sneakers);
            }

            cursor.close();
        } catch (SQLiteException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

        confirmacion.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmacion.show();

    }

    /**
     * Rellena un listview a partir de una consulta de la BD
     * @param listView
     */
    public void llenarListView(ListView listView) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, nombre, duracion, calorias_quemadas from ejercicio", null);

            CursorAdapterRutina cursoradap = new CursorAdapterRutina(this, cursor);

            listView.setAdapter(cursoradap);
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }

    }

    /**
     * Añade una rutina a la tabla de ejercicios realizados en el día
     * @param view
     */
    public void confirmarConsumir(View view) {
        confirmacion.dismiss();
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            FitnessDbHelper.registrarEjercicioUsuario(db, id_ejercicio, this);
            db.close();

        } catch (SQLiteException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }

    /**
     * Elimina la rutina seleccionada del listview
     * @param view
     */
    public void eliminarRutina(View view) {
        confirmacion.dismiss();
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            FitnessDbHelper.eliminarRutina(db, id_ejercicio);
            db.close();
            llenarListView(list_rutinas);
            FancyToast.makeText(this, "Se elimino la rutina exitosamente", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();

        } catch (SQLiteException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
        }
    }

}

package com.example.fitnessapp.fitnessapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

import java.util.ArrayList;

public class CrearRecetaActivity extends AppCompatActivity {

    private EditText nombre;
    private String nombreReceta;
    private ArrayList<Integer> idsIngreddientes = new ArrayList<Integer>();
    private double racion;
    private ListView listaIngredientes;
    private String unidad_racion;
    private double energia;
    private double proteinas;
    private double grasas;
    private double carbs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_receta);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        nombre = findViewById(R.id.etxt_nombre);
        listaIngredientes = findViewById(R.id.lv_alimentosRecetas);

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            nombreReceta = bd.getString("nombre");
            nombre.setText(nombreReceta);

            idsIngreddientes = bd.getIntegerArrayList("ingredientes");
            llenarListaIngredientes(listaIngredientes);
            System.out.println("tamaño " + idsIngreddientes.size());

        }

    }

    public void llenarListaIngredientes(ListView listView) {
        String[] ids = new String[idsIngreddientes.size()];
        for (int i = 0; i < idsIngreddientes.size(); i++) {
            ids[i] = idsIngreddientes.get(i) + "";
        }


        listaIngredientes(listView, ids);
    }


    public void listaIngredientes(ListView listView, String[] ids) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String query = "select _id, nombre, racion_estandar, energia, image, unidad_racion from alimento where _id IN (" +
                    FitnessDbHelper.makePlaceholders(ids.length) + ")";

            Cursor cursor = db.rawQuery(query, ids);

            CursorAdapterComida cursoradap = new CursorAdapterComida(this, cursor);
            listView.setAdapter(cursoradap);
            getListViewSize(listView);
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public void botonFlotante(View view) {
        Intent intent = new Intent(this, AgregaringresienteActivity.class);
        intent.putExtra("nombre", nombre.getText().toString());
        intent.putExtra("idsIngredientes", idsIngreddientes);
        startActivity(intent);
        finish();
    }

    public void crearReceta(View view) {

        if(idsIngreddientes.size() > 0) {

            for (int i = 0; i < idsIngreddientes.size(); i++) {
                consultarAlimento(idsIngreddientes.get(i));
            }

            SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
            try {
                SQLiteDatabase db = dbhelper.getWritableDatabase();
                ContentValues values = new ContentValues();

                values.put("nombre", nombre.getText().toString());
                values.put("unidad_racion", unidad_racion);
                values.put("racion_estandar", racion);
                values.put("energia", energia);
                values.put("grasas", grasas);
                values.put("carbohidratos", carbs);
                values.put("proteinas", proteinas);
                values.put("image", R.drawable.food);

                db.insert("receta", null, values);
                db.close();


                FancyToast.makeText(CrearRecetaActivity.this, "Receta creado con éxito", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();

                nombre.setText("");
                listaIngredientes.setAdapter(null);

            } catch (SQLiteException e) {
                System.out.println(e);
                Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }else{
            FancyToast.makeText(CrearRecetaActivity.this, "Ingrese los ingredientes", FancyToast.LENGTH_SHORT, FancyToast.ERROR, false).show();

        }

    }

    public void consultarAlimento(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(CrearRecetaActivity.this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas, racion_estandar, unidad_racion from alimento where _id=" + id_alimento, null);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                double caloriasAlimento = cursor.getInt(1);
                double grasasAlimento = cursor.getInt(2);
                double carbsAlimento = cursor.getInt(3);
                double proteinasAlimento = cursor.getInt(4);
                double racionAlimento = cursor.getInt(5);


                proteinas += proteinasAlimento;
                grasas += grasasAlimento;
                carbs += carbsAlimento;
                energia += caloriasAlimento;
                racion += racionAlimento;
                unidad_racion = "gr";
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(CrearRecetaActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public static void getListViewSize(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getCount(); size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
        // print height of adapter on log
        Log.i("height of listItem:", String.valueOf(totalHeight));
    }

}

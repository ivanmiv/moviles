package com.example.fitnessapp.fitnessapp;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CursorAdapterComida extends CursorAdapter {

    private LayoutInflater cursorInflater;
    public CursorAdapterComida(Context context, Cursor cursor) {
        super(context, cursor,0);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return cursorInflater.inflate(R.layout.list_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find fields to populate in inflated template
        TextView txt_nombre = (TextView) view.findViewById(R.id.textViewNombre);
        TextView txt_aux = (TextView) view.findViewById(R.id.textViewDescripcion);
        ImageView image = (ImageView) view.findViewById(R.id.imageViewIcono);
        // Extract properties from cursor
        String nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"));
        String racion = cursor.getString(cursor.getColumnIndexOrThrow("racion_estandar"));
        String energía = cursor.getString(cursor.getColumnIndexOrThrow("energia"));
        String unidad = cursor.getString(cursor.getColumnIndexOrThrow("unidad_racion"));
        int imagen = cursor.getInt(cursor.getColumnIndexOrThrow("image"));
        // Populate fields with extracted properties

        txt_nombre.setText(nombre);
        txt_aux.setText(racion+ unidad+"/"+energía+"cal");
        image.setImageResource(imagen);
    }


}

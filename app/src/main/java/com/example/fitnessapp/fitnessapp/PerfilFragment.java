package com.example.fitnessapp.fitnessapp;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilFragment extends Fragment {


    public PerfilFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        cargarDatosPerfil(view);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.menu_perfil_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    public ArrayList<String> obtenerDatosPerfil(){
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        ArrayList<String> datosPerfil = new ArrayList<>();

        try {
            String query = "SELECT _id, sexo, altura, peso, edad, objetivo, actividad_fisica FROM informacion_personal";
            Cursor cursor = db.rawQuery(query, new String[]{});
            if(cursor.moveToNext()){
                datosPerfil.add(cursor.getString(0));
                datosPerfil.add(cursor.getString(1));
                datosPerfil.add(cursor.getString(2));
                datosPerfil.add(cursor.getString(3));
                datosPerfil.add(cursor.getString(4));
                datosPerfil.add(cursor.getString(5));
                datosPerfil.add(cursor.getString(6));
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return datosPerfil;
    }


    public void cargarDatosPerfil(View view){
        ArrayList<String> datos = obtenerDatosPerfil();

        TextView textoEstatura = view.findViewById(R.id.textv_estatura);
        TextView textoPeso = view.findViewById(R.id.textv_peso);
        TextView textoEdad = view.findViewById(R.id.textv_edad);
        TextView textoObjetivo = view.findViewById(R.id.textv_objetivo);
        TextView textoActividadFisica = view.findViewById(R.id.textv_actividad_fisica);
        CircleImageView icono = view.findViewById(R.id.imgview_icono);

        if(datos.get(1).equals("Femenino")){
            icono.setImageResource(R.drawable.woman);
        }else{
            icono.setImageResource(R.drawable.man);
        }

        textoEstatura.setText(String.format("Estatura: %sm", datos.get(2)));
        textoPeso.setText(String.format("Peso: %s kg", datos.get(3)));
        textoEdad.setText(String.format("Edad: %s años", datos.get(4)));
        textoObjetivo.setText(datos.get(5));
        textoActividadFisica.setText(datos.get(6));

    }
}

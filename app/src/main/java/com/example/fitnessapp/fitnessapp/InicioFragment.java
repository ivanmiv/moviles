package com.example.fitnessapp.fitnessapp;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class InicioFragment extends Fragment {

    private double proteinasObjetivo, grasasObjetivo, carbsObjetivo, tmb ;
    private double proteinasHoy;
    private double grasasHoy;
    private double carbsHoy;
    private double caloriasHoy = 0.0;
    private double caloriasEjercicio = 0.0;

    public InicioFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inicio, container, false);

        consultarTMB();
        sumarMacronutrientes();
        calcularPorcentajeMacronutrientes(view);

        mostrarLabelObjetivos(view);

        return view;
    }

    public void mostrarLabelObjetivos(View view){
        consultarCaloriasEjercicio();
        TextView textoCal = view.findViewById(R.id.textHomeCal);

        //Objetivo - Comida + Ejercicio = Restantes
        double restantes = tmb - caloriasHoy +caloriasEjercicio;
        String calorias = (int)tmb+"-"+(int)caloriasHoy+"+"+(int)caloriasEjercicio+"="+(int)restantes;
        textoCal.setText(calorias);
    }

    public void consultarCaloriasEjercicio(){
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(new Date());
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        try {
            String query = "SELECT a._id, calorias_quemadas FROM ejercicio a INNER JOIN ejercicio_usuario b ON a._id=b.ejercicio_id WHERE fecha=?";
            Cursor cursor = db.rawQuery(query, new String[]{hoy});
            while (cursor.moveToNext()) {
                int caloriasQuemadas = cursor.getInt(1);
                caloriasEjercicio = caloriasEjercicio + caloriasQuemadas;
            }
            /*cursor.close();*/
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void pintarBarrasProgreso(View view, int grasas, int carbs, int proteinas){
        ProgressBar proteinasBar = view.findViewById(R.id.progressProteinas);
        ProgressBar carbsBar = view.findViewById(R.id.progressCarbs);
        ProgressBar grasasBar = view.findViewById(R.id.progressgrasas);

        proteinasBar.setProgress(proteinas);
        carbsBar.setProgress(carbs);
        grasasBar.setProgress(grasas);
    }

    public void calcularPorcentajeMacronutrientes(View view){
        sumarMacronutrientes();
        sumarMacronutrientesHoy(new Date());
        double proteinas=0.0;
        double carbs=0.0;
        double grasas=0.0;

        if(proteinasObjetivo != 0.0 && carbsObjetivo != 0.0 && grasasObjetivo != 0.0) {


            proteinas = ((proteinasHoy * 4) / proteinasObjetivo) * 100;
            carbs = ((carbsHoy * 4) / carbsObjetivo) * 100;
            grasas = ((grasasHoy * 9) / grasasObjetivo) * 100;

        }
        pintarBarrasProgreso(view, (int)grasas, (int)carbs, (int)proteinas);
    }

    public void sumarMacronutrientes(){

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, objetivo from informacion_personal", null);

            if (cursor.moveToFirst()) {
                String objetivo = cursor.getString(1);
                HarrisBenedict harrisBenedict = new HarrisBenedict();
                proteinasObjetivo = tmb * harrisBenedict.getProteinas(objetivo);
                grasasObjetivo = tmb * harrisBenedict.getGrasas(objetivo);
                carbsObjetivo = tmb * harrisBenedict.getCarbohidratos(objetivo);

                System.out.println("objetivo " + objetivo);
            }
            cursor.close();

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarTMB() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, fecha_nacimiento, sexo, altura, peso, edad, actividad_fisica, objetivo from informacion_personal", null);

            if (cursor.moveToFirst()) {
                String sexo = cursor.getString(2);
                double altura = cursor.getDouble(3);
                double peso = cursor.getDouble(4);
                int edad = cursor.getInt(5);
                String actividad_fisica = cursor.getString(6);
                String objetivo = cursor.getString(7);

                HarrisBenedict harrisBenedict = new HarrisBenedict();
                tmb = harrisBenedict.CalcularTMB(sexo, altura, peso, edad, actividad_fisica, objetivo);

            }else{
                insertarMedidasPrueba();
            }
            cursor.close();

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void sumarMacronutrientesHoy(Date date) {
        ArrayList<Integer> alimentos = consultarAlimentosHoy(date);
        System.out.println("alimentos " + alimentos.size());
        ArrayList<Integer> recetas = consultarRecetasHoy(date);
        System.out.println("recetas " + recetas.size());

        if (alimentos.size() == 0 && recetas.size() == 0) {
           // FancyToast.makeText(getContext(), "No se han consumido alimentos en esa fecha", FancyToast.LENGTH_SHORT, FancyToast.WARNING, false).show();
        } else {
            for (int i = 0; i < alimentos.size(); i++) {
                System.out.println("index " + i);
                consultarAlimento(alimentos.get(i));
            }
            for (int i = 0; i < recetas.size(); i++) {
                System.out.println("index " + i);
                consultarReceta(recetas.get(i));
            }
        }
    }

    public void consultarAlimento(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from alimento where _id=" + id_alimento, null);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                int caloriasAlimento = cursor.getInt(1);
                int grasasAlimento = cursor.getInt(2);
                int carbsAlimento = cursor.getInt(3);
                int proteinasAlimento = cursor.getInt(4);

                proteinasHoy += proteinasAlimento;
                grasasHoy += grasasAlimento;
                carbsHoy += carbsAlimento;
                caloriasHoy += caloriasAlimento;
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarReceta(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from receta where _id=" + id_alimento, null);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                int caloriasAlimento = cursor.getInt(1);
                int grasasAlimento = cursor.getInt(2);
                int carbsAlimento = cursor.getInt(3);
                int proteinasAlimento = cursor.getInt(4);

                proteinasHoy += proteinasAlimento;
                grasasHoy += grasasAlimento;
                carbsHoy += carbsAlimento;
                caloriasHoy += caloriasAlimento;
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public ArrayList<Integer> consultarAlimentosHoy(Date fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        ArrayList<Integer> alimentos = new ArrayList<>();

        try {
            String[] columnas = {"_id", "alimento_id"};
            String whereClause = "fecha = ?";
            String[] whereArgs = {hoy};
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("alimento_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    alimentos.add(id);
                }
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return alimentos;
    }

    public ArrayList<Integer> consultarRecetasHoy(Date fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        ArrayList<Integer> recetas = new ArrayList<>();

        try {
            String[] columnas = {"_id", "receta"};
            String whereClause = "fecha = ?";
            String[] whereArgs = {hoy};
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("receta_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    recetas.add(id);
                }
            }
            cursor.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return recetas;
    }

    //TODO elimnar este metodo
    public void insertarMedidasPrueba() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(getContext());
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            ContentValues values = new ContentValues();

            values.put("fecha_nacimiento", getDateTime());
            values.put("sexo", "F");
            values.put("altura", 165);
            values.put("peso", 54);
            values.put("edad", 21);
            values.put("actividad_fisica", "Ejercicio ligero");
            values.put("objetivo", "Bajar de peso");

            db.insert("informacion_personal", null, values);
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(getContext(), "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
    //TODO elimnar este metodo
    private static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String dateInString = "7-05-1997";
        Date date = new Date();
        try {
            date = dateFormat.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat.format(date);
    }

}

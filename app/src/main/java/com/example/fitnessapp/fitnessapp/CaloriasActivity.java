package com.example.fitnessapp.fitnessapp;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.shashank.sony.fancytoastlib.FancyToast;

public class CaloriasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calorias);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        obtenerInformacionPersonal();
    }

    /**
     * Obtiene informacion que se encuentra en la BD de la tabla información personal
     */
    public void obtenerInformacionPersonal() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, sexo, altura, peso, edad, actividad_fisica, objetivo from informacion_personal", null);

            if (cursor.moveToFirst()) {
                String sexo = cursor.getString(1);
                double altura = cursor.getDouble(2);
                double peso = cursor.getDouble(3);
                int edad = cursor.getInt(4);
                String actividad_fisica = cursor.getString(5);
                String objetivo = cursor.getString(6);

                HarrisBenedict harrisBenedict = new HarrisBenedict();
                double tmb = harrisBenedict.CalcularTMB(sexo, altura, peso, edad, actividad_fisica, objetivo);

                TextView calorias_totales = (TextView) findViewById(R.id.tv_valor_calorias);
                TextView proteinas = (TextView) findViewById(R.id.tv_valor_proteinas);
                TextView grasas = (TextView) findViewById(R.id.tv_valor_grasas);
                TextView carbohidratos = (TextView) findViewById(R.id.tv_valor_carbohidratos);
                calorias_totales.setText(String.valueOf((int) tmb));
                proteinas.setText(String.valueOf(harrisBenedict.getProteinas(objetivo) * 100) + "%");
                grasas.setText(String.valueOf(harrisBenedict.getGrasas(objetivo) * 100) + "%");
                carbohidratos.setText(String.valueOf(harrisBenedict.getCarbohidratos(objetivo) * 100) + "%");

            }
            cursor.close();
            db.close();




            cursor.close();
        } catch (SQLiteException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

        }


    }


}

package com.example.fitnessapp.fitnessapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class OpcionesFragment extends Fragment {
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private OptionsAdapter optionsAdapter;
    private ArrayList<Opcion> opciones;


    public OpcionesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_opciones, container, false);
        this.opciones = getAllOpciones();
        this.recyclerView = (RecyclerView) v.findViewById(R.id.recyclerOpciones);
        //Indica que el contenido no cambiará el tamaño del layout del RecyclerView (Mejor rendimiento)
        this.recyclerView.setHasFixedSize(true);
        this.layoutManager = new LinearLayoutManager(getActivity());
        //Lista vertical
        this.layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        this.recyclerView.setLayoutManager(layoutManager);
        this.optionsAdapter  = new OptionsAdapter(opciones);
        this.recyclerView.setAdapter(optionsAdapter);



        return v;
    }

    private ArrayList<Opcion> getAllOpciones() {
        ArrayList<Opcion> list = new ArrayList<Opcion>();
        //Ingresar aqui las opciones del menú
        list.add(new Opcion("Diario", "Diario de alimentación", R.drawable.calendar));
        list.add(new Opcion("Crear alimento", "Crea tu propio alimento", R.drawable.apple));
        list.add(new Opcion("Crear receta","Crea tu ropia receta", R.drawable.diet));
        list.add(new Opcion("Crear rutina ","Crea tu propia rutina", R.drawable.watch));
        list.add(new Opcion("Calorias y macronutrientes", "información sobre macronutrientes", R.drawable.suplement));
        //list.add(new Opcion("Consumo macronutrientes", "Ciudad uva", R.drawable.balance));
        //list.add(new Opcion("Calorias quemadas", "Ciudad uva", R.drawable.scales));
        return list;
    }

}

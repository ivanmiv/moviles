package com.example.fitnessapp.fitnessapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by john on 10/06/18.
 */

public class CursorAdapterRutina extends CursorAdapter {
    private LayoutInflater cursorInflater;

    public CursorAdapterRutina(Context context, Cursor cursor) {
        super(context, cursor, 0);
        cursorInflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return cursorInflater.inflate(R.layout.list_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // Find fields to populate in inflated template
        TextView txt_nombre = (TextView) view.findViewById(R.id.textViewNombre);
        TextView txt_aux = (TextView) view.findViewById(R.id.textViewDescripcion);
        ImageView image = (ImageView) view.findViewById(R.id.imageViewIcono);
        // Extract properties from cursor
        String nombre = cursor.getString(cursor.getColumnIndexOrThrow("nombre"));
        String duracion = cursor.getString(cursor.getColumnIndexOrThrow("duracion"));
        String calorias = cursor.getString(cursor.getColumnIndexOrThrow("calorias_quemadas"));
        // Populate fields with extracted properties

        txt_nombre.setText(nombre);
        txt_aux.setText("Duración: " + duracion + " min\nCalorias: " + calorias);
        image.setImageResource(R.drawable.sneakers);
    }
}

package com.example.fitnessapp.fitnessapp;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sara on 28/04/2018
 */

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ViewHolder> {

    protected ArrayList<Opcion> items;

    /**
     * Constructor
     * @param items
     */
    public OptionsAdapter( ArrayList<Opcion> items) {
        this.items = items;
    }

    /**
     * Se crea la vista que se le pasara al ViewHolder
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public OptionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    /*
     * Setea los valores para cada uno de los items del recycler view
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull OptionsAdapter.ViewHolder holder, final int position) {
        holder.nombre.setText(items.get(position).getNombre());
        holder.descripcion.setText(items.get(position).getDescripcion());
        holder.icono.setImageResource(items.get(position).getIcono());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (position){
                    case 0:
                        Intent intentDiario = new Intent(view.getContext(), DiarioActivity.class);
                        view.getContext().startActivity(intentDiario);
                        break;
                    case 1:
                        Intent intentCrearAlimento = new Intent(view.getContext(), CrearAlimento.class);
                        view.getContext().startActivity(intentCrearAlimento);
                        break;
                    case 2:
                        Intent intentCrearReceta = new Intent(view.getContext(), CrearRecetaActivity.class);
                        view.getContext().startActivity(intentCrearReceta);
                        break;
                    case 3:
                        Intent intentRutina = new Intent(view.getContext(), CrearRutinaActivity.class);
                        view.getContext().startActivity(intentRutina);
                        break;
                    case 4:
                        Intent intentCalorias = new Intent(view.getContext(), CaloriasActivity.class);
                        view.getContext().startActivity(intentCalorias);
                        break;
                    default:
                        break;

                }
            }
        });
    }

    /*
     * Cantidad de items del arreglo
     * @return
     */
    @Override
    public int getItemCount() {
        return items.size();
    }

    // Provee una referencia a las vistas de cada item
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre;
        public TextView descripcion;
        public ImageView icono;

        /**
         * Pasamos una vista a nuestro constructor para poder buscar los elementos usando findViewById
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            nombre = (TextView) itemView.findViewById(R.id.textViewNombre);
            descripcion = (TextView) itemView.findViewById(R.id.textViewDescripcion);
            icono = (ImageView) itemView.findViewById(R.id.imageViewIcono);
        }
    }

}

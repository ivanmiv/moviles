package com.example.fitnessapp.fitnessapp;

public class HarrisBenedict {

    final double PROTEINAS_MANTENIMIENTO = 0.25;
    final double CARBOHIDRATOS_MANTENIMIENTO = 0.45;
    final double GRASAS_MANTENIMIENTO = 0.3;

    final double PROTEINAS_SUBIR_PESO = 0.25;
    final double CARBOHIDRATOS_SUBIR_PESO= 0.5;
    final double GRASAS_SUBIR_PESO = 0.25;

    final double PROTEINAS_BAJAR_PESO = 0.35;
    final double CARBOHIDRATOS_BAJAR_PESO= 0.45;
    final double GRASAS_BAJAR_PESO = 0.20;

    public HarrisBenedict() {
    }

    public double getProteinas(String objetivo){
        if(objetivo.equalsIgnoreCase("Bajar de peso")){
            return PROTEINAS_BAJAR_PESO;
        }else if(objetivo.equalsIgnoreCase("Subir de peso")){
            return PROTEINAS_SUBIR_PESO;
        }else{
            return PROTEINAS_MANTENIMIENTO;
        }
    }

    public double getCarbohidratos(String objetivo){
        if(objetivo.equalsIgnoreCase("Bajar de peso")){
            return CARBOHIDRATOS_BAJAR_PESO;
        }else if(objetivo.equalsIgnoreCase("Subir de peso")){
            return CARBOHIDRATOS_SUBIR_PESO;
        }else{
            return CARBOHIDRATOS_MANTENIMIENTO;
        }
    }

    public double getGrasas(String objetivo){
        if(objetivo.equalsIgnoreCase("Bajar de peso")){
            return GRASAS_BAJAR_PESO;
        }else if(objetivo.equalsIgnoreCase("Subir de peso")){
            return GRASAS_SUBIR_PESO;
        }else{
            return GRASAS_MANTENIMIENTO;
        }
    }

    public double CalcularTMB(String sexo, double altura, double peso, int edad, String actividadFisica, String objetivo) {
        double tmb;
        if (sexo == "M") {
            tmb = 10 * peso + 6.25 * altura - 5 * edad + 5;
        } else {
            tmb = 10 * peso + 6.25 * altura - 5 * edad - 16;
        }

        //Se modifica el tmb segun la actividad física
        if (actividadFisica == "Poco o ningún ejercicio"){
            tmb = tmb * 1.2;
        } else if (actividadFisica == "Ejercicio ligero"){
            tmb = tmb * 1.375;
        } else if (actividadFisica == "Ejercicio moderado"){
            tmb = tmb * 1.55;
        } else if (actividadFisica == "Ejercicio fuerte"){
            tmb = tmb * 1.725;
        }else if (actividadFisica == "Ejercicio muy fuerte"){
            tmb = tmb * 1.9;
        }

        //Se modifica el tmb segun el objetivo
        if(objetivo=="Bajar de peso"){
            tmb = tmb - (0.15*tmb);
        }else if(objetivo=="Subir de peso"){
            tmb = tmb + (0.15*tmb);
        }

        return tmb;
    }
}

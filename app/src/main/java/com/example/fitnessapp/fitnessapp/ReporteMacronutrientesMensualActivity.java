package com.example.fitnessapp.fitnessapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ReporteMacronutrientesMensualActivity extends AppCompatActivity {

    private float proteinasSemana;
    private float grasasSemana;
    private float carbsSemana;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte_macronutrientes_mensual);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_charts_mensual);
        //Indica que el contenido no cambiará el tamaño del layout del RecyclerView (Mejor rendimiento)
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        ArrayList<BarData> list = generarDatosReporte();

        recyclerView.setAdapter(new BarChartAdapter(list,1));
    }

    public void sumarMacronutrientesRangoFechas(Date fecha_inicio, Date fecha_fin) {
        ArrayList<Integer> alimentos = consultarAlimentosRangoFechas(fecha_inicio, fecha_fin);
        System.out.println("alimentos " + alimentos.size());
        ArrayList<Integer> recetas = consultarRecetasRangoFechas(fecha_inicio, fecha_fin);
        System.out.println("recetas " + recetas.size());

        if (alimentos.size() == 0 && recetas.size() == 0) {
            //FancyToast.makeText(ReporteMacronutrientesMensualActivity.this, "No se han consumido alimentos en esa fecha", FancyToast.LENGTH_SHORT, FancyToast.WARNING, false).show();
        } else {
            for (int i = 0; i < alimentos.size(); i++) {
                System.out.println("index " + i);

                consultarAlimento(alimentos.get(i));
                System.out.println(proteinasSemana+" "+carbsSemana+" "+grasasSemana);

            }
            for (int i = 0; i < recetas.size(); i++) {
                System.out.println("index " + i);
                consultarReceta(recetas.get(i));
            }
        }
    }

    public void consultarAlimento(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(ReporteMacronutrientesMensualActivity.this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from alimento where _id=?", whereArgs);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                float grasasAlimento = cursor.getFloat(2);
                float carbsAlimento = cursor.getFloat(3);
                float proteinasAlimento = cursor.getFloat(4);

                proteinasSemana = proteinasSemana + proteinasAlimento;
                grasasSemana = grasasSemana + grasasAlimento;
                carbsSemana = carbsSemana + carbsAlimento;
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(ReporteMacronutrientesMensualActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarReceta(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(ReporteMacronutrientesMensualActivity.this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from receta where _id=" + id_alimento, null);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                int caloriasAlimento = cursor.getInt(1);
                int grasasAlimento = cursor.getInt(2);
                int carbsAlimento = cursor.getInt(3);
                int proteinasAlimento = cursor.getInt(4);

                proteinasSemana += proteinasAlimento;
                grasasSemana += grasasAlimento;
                carbsSemana += carbsAlimento;
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(ReporteMacronutrientesMensualActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    private ArrayList<String> obtenerFechasRango(Date fecha_inicio, Date fecha_fin){
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        ArrayList<String> fechas = new ArrayList<>();
        String inicio = dateFormat.format(fecha_inicio);
        String fin = dateFormat.format(fecha_fin);
        Date fecha_inicial = fecha_inicio;
        Date fecha_final = fecha_fin;

        fechas.add(inicio);
        while (fecha_inicial.after(fecha_final)){
            Calendar c = Calendar.getInstance();
            c.setTime(fecha_inicial);
            c.add(Calendar.DATE, -1);
            Date resultdate = new Date(c.getTimeInMillis());
            fecha_inicial = resultdate;

            fechas.add(dateFormat.format(fecha_inicial));
        }
        return fechas;
    }

    public ArrayList<Integer> consultarAlimentosRangoFechas(Date fecha_inicio, Date fecha_fin) {

        ArrayList<String> fechas = obtenerFechasRango(fecha_inicio,fecha_fin);

        String[] array_fechas = new String[fechas.size()];

        for (int i = 0; i< fechas.size(); i++){
            try {
                array_fechas[i] = fechas.get(i).toString();
            } catch (NullPointerException ex) {
                array_fechas[i] = "";
            }
        }


        SQLiteOpenHelper dbhelper = new FitnessDbHelper(ReporteMacronutrientesMensualActivity.this);
        ArrayList<Integer> alimentos = new ArrayList<>();



        try {
            String[] columnas = {"_id", "alimento_id"};
            String whereClause = "fecha IN ("+FitnessDbHelper.makePlaceholders(fechas.size())+")";
            String[] whereArgs = array_fechas;
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("alimento_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    alimentos.add(id);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(ReporteMacronutrientesMensualActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return alimentos;
    }

    public ArrayList<Integer> consultarRecetasRangoFechas(Date fecha_inicio, Date fecha_fin) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String inicio = dateFormat.format(fecha_inicio);
        String fin = dateFormat.format(fecha_fin);
        ArrayList<String> fechas = obtenerFechasRango(fecha_inicio,fecha_fin);

        String[] array_fechas = new String[fechas.size()];

        for (int i = 0; i< fechas.size(); i++){
            try {
                array_fechas[i] = fechas.get(i).toString();
            } catch (NullPointerException ex) {
                array_fechas[i] = "";
            }
        }
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(ReporteMacronutrientesMensualActivity.this);
        ArrayList<Integer> recetas = new ArrayList<>();

        try {
            String[] columnas = {"_id", "receta"};
            String whereClause = "fecha IN ("+FitnessDbHelper.makePlaceholders(fechas.size())+")";
            String[] whereArgs = array_fechas;
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("receta_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    recetas.add(id);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(ReporteMacronutrientesMensualActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return recetas;
    }

    public ArrayList<Float> obtenerCaloriasFechas(Date fecha_inicio, Date fecha_fin){


        Float calCarbs;
        Float calProteinas;
        Float calGrasas;
        sumarMacronutrientesRangoFechas(fecha_inicio,fecha_fin);
        calCarbs = carbsSemana * 4.0f;
        calProteinas = proteinasSemana * 4.0f;
        calGrasas = grasasSemana * 9.0f;
        carbsSemana = 0;
        proteinasSemana = 0;
        grasasSemana = 0;
        ArrayList<Float> resultado = new ArrayList<>();
        resultado.add(0f);
        resultado.add(calCarbs);
        resultado.add(calProteinas);
        resultado.add(calGrasas);
        System.out.println(resultado);
        return resultado;
    }



    private ArrayList<BarData> generarDatosReporte() {
        SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd", Locale.getDefault());

        ArrayList<BarData> list = new ArrayList<BarData>();

        ArrayList<ArrayList<Float>> datos = new ArrayList<>();
        ArrayList<String> fechas = new ArrayList<>();

        Date fecha_inicial = new Date();
        Date fecha_final = new Date();
        Calendar c = Calendar.getInstance();

        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        Date resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;



        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_inicial = fecha_final;
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;



        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        c = Calendar.getInstance();
        c.setTime(fecha_inicial);
        c.add(Calendar.DATE, -30);
        resultdate = new Date(c.getTimeInMillis());
        fecha_final = resultdate;

        datos.add(obtenerCaloriasFechas(fecha_inicial,fecha_final));
        fechas.add(dateFormat.format(fecha_inicial)+" a "+dateFormat.format(fecha_final) );
        fecha_inicial = fecha_final;


        for (int i = 0; i < datos.size(); i++) {
            ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
            for (int j = 0; j < datos.get(i).size(); j++) {
                entries.add(new BarEntry(j, datos.get(i).get(j)));
            }

            BarDataSet d = new BarDataSet(entries, fechas.get(i));
            d.setColors(ColorTemplate.VORDIPLOM_COLORS);
            d.setBarShadowColor(Color.rgb(203, 203, 203));

            ArrayList<IBarDataSet> sets = new ArrayList<IBarDataSet>();
            sets.add(d);

            BarData cd = new BarData(sets);
            cd.setBarWidth(0.9f);
            list.add(cd);
        }

        return list;
    }


}




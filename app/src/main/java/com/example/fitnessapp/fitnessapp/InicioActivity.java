package com.example.fitnessapp.fitnessapp;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class InicioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        setContentView(R.layout.activity_inicio);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SectionsPagerAdapter adapterPager = new SectionsPagerAdapter(getSupportFragmentManager());
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapterPager);

        TabLayout tabs = (TabLayout)findViewById(R.id.tabs);
        tabs.setupWithViewPager(pager);
    }

    /**
     * Permite posicionar el menú en la actividad
     * @param menu
     * @return
     */
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.ingresarRutina:
                Intent intent = new Intent(this,IngresarRutina.class);
                startActivity(intent);
                return true;
            case R.id.verPerfil:
                Intent intent_perfil = new Intent(this,EditarMedidasActivity.class);
                startActivity(intent_perfil);
                return true;
            case R.id.ingresarComida:
                Intent intentComida = new Intent(this,IngresarComida.class);
                startActivity(intentComida);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch(position){
                case 0: return getResources().getText(R.string.tabInicio);
                case 1: return getResources().getText(R.string.tabOpciones);
                case 2: return getResources().getText(R.string.tabPerfil);
                case 3: return getResources().getText(R.string.tabReportes);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0: return new InicioFragment();
                case 1: return new OpcionesFragment();
                case 2: return new PerfilFragment();
                case 3: return new ReportesFragment();
            }
            return null;
        }
    }

}

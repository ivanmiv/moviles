package com.example.fitnessapp.fitnessapp;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;
import com.shashank.sony.fancytoastlib.FancyToast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DiarioActivity extends AppCompatActivity {

    private double proteinasHoy;
    private double grasasHoy;
    private double carbsHoy;
    private MaterialCalendarView materialCalendarView;
    private double caloriasHoy = 0.0;
    private double caloriasEjercicio = 0.0;
    private double proteinasObjetivotmb, grasasObjetivotmb, carbsObjetivotmb ;
    private double tmb = 0.0;
    private PieChart chart;
    private ListView listDesayuno, listAlmuerzo, listCena, listSnacks;
    private ListView listDesayunoReceta , listaAlmuerzoReceta, listaCenaReceta, listaSnacksReceta;
    private TextView calObjetivo, calConsumidas, protObjetivo, protConsumidas, carbsObjetivo, carbsConsumidas, grasasObjetivo, grasasConsumidas;
    private int id_alimentoConsumido;
    private Date dateConsumo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        calObjetivo = findViewById(R.id.txt_calObjetivo);
        calConsumidas = findViewById(R.id.txt_calConsumidas);
        protObjetivo = findViewById(R.id.txt_protObjetivo);
        protConsumidas = findViewById(R.id.txt_protConsumidas);
        carbsObjetivo = findViewById(R.id.txt_carbsObjetivo);
        carbsConsumidas = findViewById(R.id.txt_carbsConsumidos);
        grasasObjetivo = findViewById(R.id.txt_grasasObjetivo);
        grasasConsumidas = findViewById(R.id.txt_grasasConsumidos);


        materialCalendarView = (MaterialCalendarView) findViewById(R.id.calendarView);
        //materialCalendarView.setCurrentDate(CalendarDay.today());
        materialCalendarView.setDateSelected(CalendarDay.today(), true);

        materialCalendarView.state().edit()
                .setCalendarDisplayMode(CalendarMode.WEEKS)
                .commit();

        listDesayuno = findViewById(R.id.lv_desayuno);
        listAlmuerzo = findViewById(R.id.lv_almuerzo);
        listCena = findViewById(R.id.lv_cena);
        listSnacks = findViewById(R.id.lv_snacks);

        listDesayunoReceta = findViewById(R.id.lv_desayunoReceta);
        listaAlmuerzoReceta = findViewById(R.id.lv_almuerzoReceta);
        listaCenaReceta = findViewById(R.id.lv_CenaReceta);
        listaSnacksReceta = findViewById(R.id.lv_SnacksReceta);

        chart = (PieChart) findViewById(R.id.pieChartDiario);
        pintarPie(chart, new Date());

        llenarinformacionCalorias(new Date());

        mostrarComidas(new Date(), "Desayuno", listDesayuno);
        mostrarComidas(new Date(), "Almuerzo", listAlmuerzo);
        mostrarComidas(new Date(), "Cena", listCena);
        mostrarComidas(new Date(), "Snacks", listSnacks);

        mostrarRecetas(new Date(), "Desayuno", listDesayunoReceta);
        mostrarRecetas(new Date(), "Almuerzo", listaAlmuerzoReceta);
        mostrarRecetas(new Date(), "Cena", listaCenaReceta);
        mostrarRecetas(new Date(), "Snacks", listaSnacksReceta);


        materialCalendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                proteinasHoy = 0.0;
                grasasHoy = 0.0;
                carbsHoy = 0.0;
                caloriasHoy = 0.0;
                caloriasEjercicio=0.0;
                pintarPie(chart, date.getDate());

                listDesayuno.setAdapter(null);
                listAlmuerzo.setAdapter(null);
                listCena.setAdapter(null);
                listSnacks.setAdapter(null);

                mostrarComidas(date.getDate(), "Desayuno", listDesayuno);
                mostrarComidas(date.getDate(), "Almuerzo", listAlmuerzo);
                mostrarComidas(date.getDate(), "Cena", listCena);
                mostrarComidas(date.getDate(), "Snacks", listSnacks);

                listDesayunoReceta.setAdapter(null);
                listaAlmuerzoReceta.setAdapter(null);
                listaCenaReceta.setAdapter(null);
                listaSnacksReceta.setAdapter(null);

                mostrarRecetas(date.getDate(), "Desayuno", listDesayunoReceta);
                mostrarRecetas(date.getDate(), "Almuerzo", listaAlmuerzoReceta);
                mostrarRecetas(date.getDate(), "Cena", listaCenaReceta);
                mostrarRecetas(date.getDate(), "Snacks", listaSnacksReceta);

                llenarinformacionCalorias(date.getDate());

            }
        });

        listDesayuno.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarAlimento(parent, position, "Desayuno", listDesayuno);
            }
        });

        listAlmuerzo.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarAlimento(parent, position, "Almuerzo", listAlmuerzo);
            }
        });

        listCena.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarAlimento(parent, position, "Cena", listCena);
            }
        });

        listSnacks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarAlimento(parent, position, "Snacks", listSnacks);
            }
        });
        
        listDesayunoReceta.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarReceta(parent, position, "Desayuno", listDesayunoReceta);
            }
        });

        listaAlmuerzoReceta.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarReceta(parent, position, "Almuerzo", listaAlmuerzoReceta);
            }
        });

        listaCenaReceta.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarReceta(parent, position, "Cena", listaCenaReceta);
            }
        });

        listaSnacksReceta.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long l) {
                return listenerEliminarReceta(parent, position, "Snacks", listaSnacksReceta);
            }
        });

    }

    public boolean listenerEliminarAlimento(AdapterView<?> parent, int position, final String horario, final ListView listaAlimentos) {
        Cursor cursor = (Cursor) parent.getItemAtPosition(position);
        id_alimentoConsumido = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        dateConsumo = materialCalendarView.getSelectedDate().getDate();

        AlertDialog.Builder ad = new AlertDialog.Builder(DiarioActivity.this);
        ad.setTitle("Eliminar alimento");
        ad.setMessage("¿Está seguro de que desea eliminar el alimento?");

        ad.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Delete of record from Database and List view.
                String fechaFormato = getDateTimeFormat(dateConsumo);
                int index = buscarAlimentoConsumido(id_alimentoConsumido, fechaFormato, horario);

                eliminarAlimento(index);
                FancyToast.makeText(DiarioActivity.this, "Alimento eliminado con éxito", FancyToast.LENGTH_SHORT, FancyToast.INFO, false).show();

                listaAlimentos.setAdapter(null);
                mostrarComidas(dateConsumo, horario, listaAlimentos);

                proteinasHoy = 0.0;
                grasasHoy = 0.0;
                carbsHoy = 0.0;
                caloriasHoy = 0.0;
                pintarPie(chart, dateConsumo);

                dialog.dismiss();
            }
        });

        ad.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ad.show();
        cursor.close();
        return false;
    }

    public boolean listenerEliminarReceta(AdapterView<?> parent, int position, final String horario, final ListView listaAlimentos) {
        Cursor cursor = (Cursor) parent.getItemAtPosition(position);
        id_alimentoConsumido = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        dateConsumo = materialCalendarView.getSelectedDate().getDate();

        AlertDialog.Builder ad = new AlertDialog.Builder(DiarioActivity.this);
        ad.setTitle("Eliminar alimento");
        ad.setMessage("¿Está seguro de que desea eliminar la receta?");

        ad.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Delete of record from Database and List view.
                String fechaFormato = getDateTimeFormat(dateConsumo);
                int index = buscarRecetaConsumido(id_alimentoConsumido, fechaFormato, horario);

                eliminarReceta(index);
                FancyToast.makeText(DiarioActivity.this, "Receta eliminada con éxito", FancyToast.LENGTH_SHORT, FancyToast.INFO, false).show();

                listaAlimentos.setAdapter(null);
                mostrarRecetas(dateConsumo, horario, listaAlimentos);

                proteinasHoy = 0.0;
                grasasHoy = 0.0;
                carbsHoy = 0.0;
                caloriasHoy = 0.0;
                pintarPie(chart, dateConsumo);

                dialog.dismiss();
            }
        });

        ad.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        ad.show();
        cursor.close();
        return false;
    }

    public void mostrarComidas(Date fecha, String horario, ListView lista) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        try {
            String query = "SELECT a._id, nombre, racion_estandar, energia, image, unidad_racion, horario_comida FROM alimento a INNER JOIN alimento_usuario b ON a._id=b.alimento_id WHERE fecha=? AND horario_comida=?";

            Cursor cursor = db.rawQuery(query, new String[]{hoy, horario});

            if (cursor.moveToFirst()) {

                CursorAdapterComida cursoradap = new CursorAdapterComida(this, cursor);
                lista.setAdapter(cursoradap);
                getListViewSize(lista);

            } else {
                ViewGroup.LayoutParams params = lista.getLayoutParams();
                params.height = 0;
                lista.setLayoutParams(params);
            }
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void mostrarRecetas(Date fecha, String horario, ListView lista) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        try {
            String query = "SELECT a._id, nombre, racion_estandar, energia, image, unidad_racion, horario_receta FROM receta a INNER JOIN receta_usuario b ON a._id=b.receta WHERE fecha=? AND horario_receta=?";

            Cursor cursor = db.rawQuery(query, new String[]{hoy, horario});

            if (cursor.moveToFirst()) {

                CursorAdapterComida cursoradap = new CursorAdapterComida(this, cursor);
                lista.setAdapter(cursoradap);
                getListViewSize(lista);

            } else {
                ViewGroup.LayoutParams params = lista.getLayoutParams();
                params.height = 0;
                lista.setLayoutParams(params);
            }
            /*cursor.close();*/

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void pintarPie(PieChart chart, Date date) {

        sumarMacronutrientesHoy(date);
        List<PieEntry> entries = new ArrayList<>();

        //Se convierte los macronutrientes de g -> cal
        double calCarbs = carbsHoy * 4.0;
        double calProteinas = proteinasHoy * 4.0;
        double calGrasas = grasasHoy * 9.0;

        double porcentajeCarbs = (calCarbs / caloriasHoy) * 100.0;
        double porcentajeProteinas = (calProteinas / caloriasHoy) * 100;
        double porcentajeGrasas = (calGrasas / caloriasHoy) * 100;

        entries.add(new PieEntry((float) porcentajeProteinas, "Proteínas"));
        entries.add(new PieEntry((float) porcentajeGrasas, "Grasas"));
        entries.add(new PieEntry((float) porcentajeCarbs, "Carbs"));

        PieDataSet set = new PieDataSet(entries, "Macronutrientes");
        set.setColors(ColorTemplate.MATERIAL_COLORS);
        PieData data = new PieData(set);
        chart.setData(data);
        chart.invalidate(); // refresh
        chart.animateX(1000);
    }

    public void sumarMacronutrientesHoy(Date date) {
        ArrayList<Integer> alimentos = consultarAlimentosHoy(date);
        ArrayList<Integer> recetas = consultarRecetasHoy(date);

        if (alimentos.size() == 0 && recetas.size() == 0) {
            //FancyToast.makeText(DiarioActivity.this, "No se han consumido alimentos en esa fecha", FancyToast.LENGTH_SHORT, FancyToast.WARNING, false).show();
        } else {
            for (int i = 0; i < alimentos.size(); i++) {
                consultarAlimento(alimentos.get(i));
            }
            for (int i = 0; i < recetas.size(); i++) {
                consultarReceta(recetas.get(i));
            }
        }
    }

    public void consultarAlimento(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from alimento where _id=?", whereArgs);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                double caloriasAlimento = cursor.getDouble(1);
                double grasasAlimento = cursor.getDouble(2);
                double carbsAlimento = cursor.getDouble(3);
                double proteinasAlimento = cursor.getDouble(4);

                proteinasHoy = proteinasHoy + proteinasAlimento;
                grasasHoy = grasasHoy + grasasAlimento;
                carbsHoy = carbsHoy+ carbsAlimento;
                caloriasHoy = caloriasHoy+ caloriasAlimento;
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarReceta(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            String[] columnas = {"_id", "energia", "grasas", "carbohidratos", "proteinas"};
            String whereClause = "_id=?";
            String[] whereArgs = {id_alimento + ""};

            Cursor cursor = db.rawQuery("select _id, energia, grasas, carbohidratos, proteinas from receta where _id=" + id_alimento, null);
            //Cursor cursor = db.query("alimento", columnas,whereClause, whereArgs , null, null, null, null);

            if (cursor.moveToFirst()) {
                int caloriasAlimento = cursor.getInt(1);
                int grasasAlimento = cursor.getInt(2);
                int carbsAlimento = cursor.getInt(3);
                int proteinasAlimento = cursor.getInt(4);

                proteinasHoy += proteinasAlimento;
                grasasHoy += grasasAlimento;
                carbsHoy += carbsAlimento;
                caloriasHoy += caloriasAlimento;
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public ArrayList<Integer> consultarAlimentosHoy(Date fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);
        ArrayList<Integer> alimentos = new ArrayList<>();

        try {
            String[] columnas = {"_id", "alimento_id"};
            String whereClause = "fecha = ?";
            String[] whereArgs = {hoy};
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("alimento_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    alimentos.add(id);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return alimentos;
    }

    public ArrayList<Integer> consultarRecetasHoy(Date fecha) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(fecha);

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(DiarioActivity.this);
        ArrayList<Integer> recetas = new ArrayList<>();

        try {
            String[] columnas = {"_id", "receta"};
            String whereClause = "fecha = ?";
            String[] whereArgs = {hoy};
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.query("receta_usuario", columnas, whereClause, whereArgs, null, null, null, null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(1);
                    recetas.add(id);
                }
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(DiarioActivity.this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return recetas;
    }

    public int buscarAlimentoConsumido(int id_alimento, String date, String horario) {
        int indice = -1;

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            System.out.println(date + " " + horario + " " + id_alimento);

            Cursor cursor = db.rawQuery("SELECT _id FROM alimento_usuario WHERE fecha=? AND horario_comida=? AND alimento_id=?", new String[]{date, horario, id_alimento + ""});

            if (cursor.moveToFirst()) {
                int alimentoConsumida = cursor.getInt(0);
                indice = alimentoConsumida;
                return indice;
            }
            db.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return indice;
    }

    public int buscarRecetaConsumido(int id_alimento, String date, String horario) {
        int indice = -1;

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            System.out.println(date + " " + horario + " " + id_alimento);

            Cursor cursor = db.rawQuery("SELECT _id FROM receta_usuario WHERE fecha=? AND horario_receta=? AND receta=?", new String[]{date, horario, id_alimento + ""});

            if (cursor.moveToFirst()) {
                int recetaConsumida = cursor.getInt(0);
                indice = recetaConsumida;
                return indice;
            }
            db.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return indice;
    }

    public void eliminarAlimento(int id_alimento) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            /*Cursor cursor3 = db.rawQuery("SELECT * FROM alimento_usuario where _id=?", new String[]{id_alimento + ""});
            System.out.println("Consulta " + cursor3.getCount());*/

            db.delete("alimento_usuario", "_id=?", new String[]{id_alimento + ""});

            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void eliminarReceta(int id_receta) {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            /*Cursor cursor3 = db.rawQuery("SELECT * FROM receta_usuario where _id=?", new String[]{id_receta + ""});*/
            db.delete("receta_usuario", "_id=?", new String[]{id_receta + ""});
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarTMB() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, fecha_nacimiento, sexo, altura, peso, edad, actividad_fisica, objetivo from informacion_personal", null);

            if (cursor.moveToFirst()) {
                String sexo = cursor.getString(2);
                double altura = cursor.getDouble(3);
                double peso = cursor.getDouble(4);
                int edad = cursor.getInt(5);
                String actividad_fisica = cursor.getString(6);
                String objetivo = cursor.getString(7);

                HarrisBenedict harrisBenedict = new HarrisBenedict();
                tmb = harrisBenedict.CalcularTMB(sexo, altura, peso, edad, actividad_fisica, objetivo);

            }
            cursor.close();
            db.close();

        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void consultarCaloriasEjercicio(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        String hoy = dateFormat.format(date);
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        try {
            String query = "SELECT a._id, calorias_quemadas FROM ejercicio a INNER JOIN ejercicio_usuario b ON a._id=b.ejercicio_id WHERE fecha=?";
            Cursor cursor = db.rawQuery(query, new String[]{hoy});
            while (cursor.moveToNext()) {
                int caloriasQuemadas = cursor.getInt(1);
                caloriasEjercicio = caloriasEjercicio + caloriasQuemadas;
            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            System.out.println(e);
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void sumarMacronutrientes(double tmbEjercicio){

        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);

        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, objetivo from informacion_personal", null);

            if (cursor.moveToFirst()) {
                String objetivo = cursor.getString(1);
                HarrisBenedict harrisBenedict = new HarrisBenedict();
                proteinasObjetivotmb = tmbEjercicio * harrisBenedict.getProteinas(objetivo);
                grasasObjetivotmb = tmbEjercicio * harrisBenedict.getGrasas(objetivo);
                carbsObjetivotmb = tmbEjercicio * harrisBenedict.getCarbohidratos(objetivo);

            }
            cursor.close();
            db.close();
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void llenarinformacionCalorias(Date date){
        consultarTMB();
        consultarCaloriasEjercicio(date);
        double objetivo = tmb + caloriasEjercicio;
        calObjetivo.setText((int)objetivo+"");
        calConsumidas.setText((int)caloriasHoy+"");

        protConsumidas.setText((int)(proteinasHoy*4)+"");
        carbsConsumidas.setText((int)(carbsHoy*4)+"");
        grasasConsumidas.setText((int)(grasasHoy*9)+"");

        sumarMacronutrientes(objetivo);

        protObjetivo.setText((int)proteinasObjetivotmb+"");
        carbsObjetivo.setText((int)carbsObjetivotmb+"");
        grasasObjetivo.setText((int)grasasObjetivotmb+"");
    }


    //Para registrar fecha actual
    private static String getDateTimeFormat(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        System.out.println("ingresar " + dateFormat.format(date));
        return dateFormat.format(date);
    }

    public static void getListViewSize(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            //do nothing return null
            return;
        }
        //set listAdapter in loop for getting final size
        int totalHeight = 0;
        for (int size = 0; size < myListAdapter.getCount(); size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //setting listview item in adapter
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (myListAdapter.getCount() - 1));
        myListView.setLayoutParams(params);
        // print height of adapter on log
        Log.i("height of listItem:", String.valueOf(totalHeight));
    }

}




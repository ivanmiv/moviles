package com.example.fitnessapp.fitnessapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.shashank.sony.fancytoastlib.FancyToast;

public class EditarMedidasActivity extends AppCompatActivity {

    private String seleccion_objetivo;
    private String seleccion_actividad;
    private int id_informacion;
    private Spinner spinner_objetivo;
    private Spinner spinner_actividad_fisica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_medidas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        spinner_objetivo = findViewById(R.id.spinner_editar_objetivo);
        spinner_actividad_fisica = findViewById(R.id.spinner_editar_actividad_fisica);

        ArrayAdapter<CharSequence> adapter_objetivo = ArrayAdapter.createFromResource(this,
                R.array.objetivoOptions, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter_actividad = ArrayAdapter.createFromResource(this,
                R.array.actividadFisicaOptions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter_objetivo.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter_actividad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_objetivo.setAdapter(adapter_objetivo);
        spinner_actividad_fisica.setAdapter(adapter_actividad);

        spinner_objetivo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                seleccion_objetivo = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner_actividad_fisica.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                seleccion_actividad = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        obtenerInformacionPersonal();
    }

    /**
     * Obtiene el id que se encuentre en la BD de la tabla información personal
     */
    public void obtenerInformacionPersonal() {
        SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
        try {
            SQLiteDatabase db = dbhelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("select _id, objetivo, actividad_fisica, peso from informacion_personal", null);
            cursor.moveToFirst();
            id_informacion = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
            ArrayAdapter<CharSequence> adapter_objetivo = (ArrayAdapter<CharSequence>) spinner_objetivo.getAdapter();
            spinner_objetivo.setSelection(adapter_objetivo.getPosition(cursor.getString(1)));

            ArrayAdapter<CharSequence> adapter_actividad = (ArrayAdapter<CharSequence>) spinner_actividad_fisica.getAdapter();
            spinner_actividad_fisica.setSelection(adapter_actividad.getPosition(cursor.getString(2)));

            EditText et_peso = (EditText) findViewById(R.id.et_editar_peso);
            et_peso.setText(cursor.getString(3));


            cursor.close();
        } catch (SQLiteException e) {
            FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

        }


    }


    /**
     * Verifica si los campos estan vacios
     *
     * @return
     */
    public boolean camposVacios() {
        EditText et_peso = (EditText) findViewById(R.id.et_editar_peso);

        if (TextUtils.isEmpty(et_peso.getText())) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Limbia los campos de texto
     *
     * @return
     */
    public void limpiarCampos() {
        EditText et_peso = (EditText) findViewById(R.id.et_editar_peso);
        et_peso.setText("");
    }


    public void guardarEditarInformacion(View view) {
        if (!camposVacios()) {
            SQLiteOpenHelper dbhelper = new FitnessDbHelper(this);
            try {
                SQLiteDatabase db = dbhelper.getWritableDatabase();
                EditText et_peso = (EditText) findViewById(R.id.et_editar_peso);
                FitnessDbHelper.actualizarInformacionPersonal(db, Double.parseDouble(et_peso.getText().toString()),
                        seleccion_objetivo, seleccion_actividad, id_informacion);
                Intent intent = new Intent(this, InicioActivity.class);
                startActivity(intent);
                finish();
                FancyToast.makeText(this, "Se actualizo la información exitosamente", FancyToast.LENGTH_LONG, FancyToast.SUCCESS, false).show();

            } catch (SQLiteException e) {
                FancyToast.makeText(this, "Base de datos no disponible", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();
            }
        } else {
            FancyToast.makeText(this, "No pueden haber campos en blanco", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();
        }
    }


}
